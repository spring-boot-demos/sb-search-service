package com.example.sbsearchservice.model;

import lombok.Data;

@Data
public class Employee {
    private Long employeeId;
    private String name;
    private String practiceArea;
    private String designation;
    private String companyInfo;
}
